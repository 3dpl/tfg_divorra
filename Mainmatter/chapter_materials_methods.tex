\chapter{Materials and Methods}
\label{cha:materials_and_methods}

\begin{chapterabstract}
	In this second chapter we detail the different materials and methods we will use in the Thesis, mainly in software, data, and hardware. The chapter is divided in: Section \ref{sec:materials_and_methods:introduction}, where the context of the material and methods is presented. Section \ref{sec:materials_and_methods:framework}, where three deep learning frameworks are studied and one is selected. Section \ref{sec:materials_and_methods:datasets}, in which common action recognition datasets are introduced. Section \ref{sec:materials_and_methods:hardware}, presenting the server where experiments are going to be run.
\end{chapterabstract}

\section{Introduction}
\label{sec:materials_and_methods:introduction}

For being able to realize this work, we need three types of resources. First, a programming framework that allows to increase the level of abstraction at the time of implementing deep learning models, training them and testing them. Although we could ignore it and program at a lower level, we would loss the focus of the main project task, as well as it would take from us an unnecessary large amount of time.

Second, we have to find suitable datasets for training models in the task action recognition. Moreover, we need one that is large enough, due to the nature of deep models and their easiness for overfitting with the training set.

Third, a powerful enough computer is needed for performing the multiple operations a deep learning system carries out. Also, it needs to have enough storage and memory for holding the used datasets.

\section{Frameworks}
\label{sec:materials_and_methods:framework}
With the raise of deep learning, easily writing code representations of models in a flexible and fast manner has become a trending necessity among researchers. For this reason, deep learning frameworks are nowadays needed. The list of them is hugely growing each year, and we need to properly focus the search on one that is adequate to our specific needs. Subsequently, we introduce three possible candidates: TensorFlow, Keras, and PyTorch.

\subsection{TensorFlow}
TensorFlow\cite{Abadi2015TensorFlow:Systems} is a library for numerical computation and using data flow graphs. Although actually, its most widespread use is the experimentation and development of deep learning models, it as well supports a wider range of machine learning algorithms, being this was the main purpose it was created for, by researchers and engineers at the Google Brain team. Its open-source nature (Apache 2.0 license) has contributed to ease the framework's improvement, thanks to the support of a large number of contributors at its official repository\footnote{\url{https://github.com/tensorflow/tensorflow}}. TensorFlow, provides a flexible and fine-grained interface, implemented in both C++ and Python. By its architectural design, allows to scalably build and deploy systems seamlessly whether it is on \ac{CPU} or \ac{GPU}.

At the core of the framework lies the capacity of expressing computations as data flow graphs (nodes represent the operations while edges the data to be consumed). Several benefits emerge from this kind of representation: Easiness to detect and parallelize independent paths in the graph. Distribute operations across different devices and machines, for example, we could be training a model at the \ac{GPU}, while performing data augmentation for the next batch on the \ac{CPU} at the same time. Automatic differentiation, which allows to compute the derivatives of a function automatically. This method is based on the idea that any kind of operation, independently of its complexity, can be expressed as a group of primitive operations (add, multiply, divide...) and basic functions (exponent, root, logarithm...). Thus, by following the data flow graph scheme, each function operation can be broken down into these components, then, by applying the chain rule successively, the derivative can be obtained. In machine learning, this characteristic is heavily used at the time of finding gradients of loss functions on backpropagation based algorithms. \newline

\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{materials_methods/tensorflow_graph}
	\caption{Computation graph of one single-layer network}
\label{fig:tensorflow_graph}
\end{figure}

\subsection{Keras}
Keras\cite{Chollet2015Keras} is a high-level deep learning framework fully written in Python developed by Fran\c{c}ois Chollet. Aimed for fast prototyping and simplifying the construction process of neural networks, facilitating the entrance of beginners to the field. For accomplishing these objectives, it acts as a front-end for three deep learning libraries, \ac{CNTK}\cite{Agarwal2015AnToolkit}, TensorFlow (which officially supports Keras) and Theano\cite{Al-Rfou2016Theano:Expressions}, allowing to interchange them easily and without almost any distinction, and being able to directly access them within Keras. Moreover the selection between \ac{GPU} or \ac{CPU} when executing the model is done, by default, in the background. Apart from the main repository\footnote{\url{https://github.com/keras-team/keras}}, the community can extend the framework through new additions at the contributions repository\footnote{\url{https://github.com/keras-team/keras-contrib}} (both, under the \ac{MIT} license).

Keras allows to create models as a sequence of layers (Sequential \ac{API}) or as a graph of them (Functional \ac{API}). The first approach is directed to creating simpler model by stacking layers linearly, obtaining a sole linear path from the input to the output. On the other hand, the latter approach enables to treat each layer as a node of a graph, being possible to have multiple entrances and exits on a model. Thus, enabling the design of more complex models without much little effort. \newline

\subsection{PyTorch}
PyTorch\cite{Paszke2017AutomaticPyTorch} is both a Python tensor computation kit and a machine learning platform, mainly aimed for research. Its development has been mainly driven by \ac{FAIR} together with many contributors at GitHub\footnote{\url{https://github.com/pytorch/pytorch}}. Although being in early beta, it is enough mature to stand out within modern deep learning frameworks. As it may be appreciable, it is well influenced by Torch\cite{Collobert2011Torch7:Learning}, a scientific computing Lua framework focused on supporting a wide range of machine learning algorithms. Specifically, PyTorch uses the same C back-ends as the ones implemented by Torch (TH, THC, THNN, THCUNN), thus obtaining a resulting performance on-par with Torch, or even better.

As a tensor kit, PyTorch offers a variated number of operations, quite similar to the ones NumPy has, being it proposed as a \ac{GPU} alternative for it. Indeed, when transforming a tensor from PyTorch to NumPy, the converted version and the original will share the same memory location, meaning that if one is modified, the other will also be. Furthermore, it also incorporates automatic differentiation.

When used for machine learning, PyTorch has a differentiable characteristic, it operates with dynamic graphs, instead of the static versions used by a great number of deep learning frameworks. Concretely, this means that instead of defining a graph, compiling it and then being hardly difficult to change its logic at runtime, PyTorch creates a new graph on each model execution, allowing to vary the interns of it each time. This has several benefits, for example, in a  network, the number and type of hidden layers could change depending on an epoch counter, flexible length sequences could be easily added for \acp{RNN} or even some parts of the network could be re-executed with different hyper-parameters until some layer outputs meet certain conditions such as expected loss or only a few number of dead neurons. This could also be the inverse, the execution of some layers could be skipped. For example, batch normalization could be avoided if an output is under a threshold mean and variance. Specifically, all this logic can be written with standard Python conditionals. Furthermore, the implementation of the models follows an object oriented approach, with design principles like inversion of control, where the network architecture is defined in a class and its parent handles all the underlying logic. These enhances extensibility and modularity. Also, layers can be added in a similar way than in Keras, following either, a sequential or functional approach. \newline

\subsection{Picking a Framework}

Since the objectives of this work are mainly related to experimentation and research of possible action recognition pipeline improvements, we need a framework that is adaptable enough, easily supports fine-grained and coarse-grained tensor operations and layers, and offers high expressibility. For these reasons we select PyTorch as the framework we are going to work with. It presents both a high level \ac{API} similar to Keras and a vast number of lower level operations. The approach it takes at the time of implementing networks as objects helps to understand and debug the model implementations. Furthermore, since it has obtained such a good reception between researchers since its first official release (at the beginning of 2017), many industries are giving PyTorch support to newly interesting tools, helping to create an ever stronger community. 

Finally, although we have seen that it is efficient enough, we can better aim at production releases of PyTorch projects thanks to its incoming integration with Caffe2\footnotemark[1] (which since less than five months ago lives inside the PyTorch project) and 1.0 release, which will introduce more efficient operations at the same time it will make the model conversions much easier.

\footnotetext[1]{\url{https://caffe2.ai/blog/2018/05/02/Caffe2_PyTorch_1_0.html}}

\section{Datasets}
\label{sec:materials_and_methods:datasets}

In this section we introduce nine action recognition datasets as well as a summary table which highlights important information about each of them. The reviewed datasets have been selected in regard of their innovation in terms of how data is acquired and presented, their scale, which has to be large enough for properly training deep networks, and the types of actions they include. 

\subsection{RGBD-HuDaAct}
\cite{Ni2011RGBD-HuDaAct:Recognition} Motivated by the limited capacity of \ac{RGB} methods at the time (2011) for the task of activity recognition (specifically in home environments with elderly people), authors present a  dataset consisting of human daily actions recorded with both depth and \ac{RGB} low cost cameras. Adding this new type of information (depth), enables to better capture the scene context as well as the motions itself, and thus reduce the recognition ambiguity that could be originated from the contemporary feature extraction processes.\footnotemark[1]

More in detail, it consists of 12 classes and more than 1000 videos, giving a total of around 46 video hours. The classes cover actions ranging from \textit{make a phone call} and \textit{take off the jacket} to \textit{mop the floor} and \textit{eat meal} or \textit{drink water}. 

\subsection{UTKinect-Action3D}
\cite{Xia2012ViewJoints} In order to test the new depth-based action recognition method developed, authors collect a dataset of \ac{RGB-D} action sequences in indoor scenarios. Moreover, skeletal joints are extracted with the help of the recording Kinect. On the other hand, actions are captured from different angles and poses in order to demonstrate the view-invariance of their method. 

Concretely, the dataset presents a collection of short videos taking from 1 to 4 seconds, with a mean of 20 videos per class in a total of 10 classes. The complete list of actions together with an example of it, can be observed in \referenced{Xia2012ViewJoints}.\footnotemark[2]

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{materials_methods/Xia2012ViewJoints}
	\caption{Sample frames for each action of UTKinect-Action3D, from left to right and top to bottom: \textit{walk}, \textit{stand up}, \textit{sit} \textit{down},
\textit{pick up}, \textit{carry}, \textit{throw}, \textit{push}, \textit{pull}, \textit{wave hands}, \textit{clap hands}. For each row: Above \ac{RGB} data, below \ac{RGB-D} data. \extracted{Xia2012ViewJoints}}
\label{fig:Xia2012ViewJoints}
\end{figure}

\subsection{UTKinect-FirstPerson}
\cite{XiaRobot-CentricVideos} Authors of this paper present two independent datasets recorded from the perspective of mobile robots. Concretely, the aim is to learn how people interact with them, since this information is of great interest at the time of developing new daily life applications that have these machines as its core, like surveillance or social care. Precisely, one robot has an humanoid appearance while the other does not. This is done in order to further explore the difference in behaviours that could arise from treating with each type of robot.

As an improvement from similar datasets of this type,  depth data is also added to the \ac{RGB} recordings for fully capturing the two type of motion information present in the dataset: (1) Ego-motion, related with the camera movement. In this case originated from both the autonomous movement and the interaction-resulting one, (2) independent motion, which is caused by the movement of subjects and objects in the scene. 

Regarding the datasets organization, they present both the \ac{RGB} and depth records as well as the skeletal joints of the subjects. They are divided into nine classes, each one with specific actions. For the humanoid robot, we can see ones like \textit{hug}, \textit{point}, \textit{punch}, or \textit{shake hands}. On the other hand, for the non-humanoid robot they include: \textit{ignore}, \textit{pass by the robot}, \textit{run away}, \textit{stand up}, or \textit{wave to the robot}.\footnotemark[3] A sample of some actions can be observed in \referenced{XiaRobot-CentricVideos}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{materials_methods/XiaRobot-CentricVideos}
	\caption{\ac{RGB} frames of the UTKinect-FirstPerson dataset. \extracted{XiaRobot-CentricVideos}}
\label{fig:XiaRobot-CentricVideos}
\end{figure}

\subsection{MHAD}
\cite{Ofli2013BerkeleyDatabase} Since many datasets rise problems like being limited to some capture modalities  and purposes, or not giving information about the hardware used for its creation (e.g, videos from YouTube), which could bias experiment results, authors propose a multi-modal database of captured actions. Moreover, in both paper and project page, they give a detailed description of how it was captured and synchronized, together with the devices used.

In particular, data is acquired from five different sources: (1) \ac{Mocap}, which allowed to extract 3D movement and skeleton information; (2) cameras: two groups where used for stereo capture, while two more aimed for recording multi-view RGB data; (3) Kinects set in opposite directions (to avoid their pattern interference) for obtaining the depth data; (4) six accelerometers attached to the \ac{Mocap} suit to obtain more precise temporal information of the actions. They were located at the ankles, wrists and hips; (5) audio through four microphones, since the combination of visual and auditive information has been proven to improve. One microphone is situated on the floor in order to obtain the audio that can be propagated through the action surface. The three other ones surrounded the motion scene. An example of the cameras and \ac{Mocap} modalities can be seen in \referenced{Ofli2013BerkeleyDatabase}.

The dataset is composed of 660 videos (with approximately 1.5 hours), having all the data sources synchronized after processing, and 11 action classes. They include: \textit{jumping}, \textit{bending}, \textit{throwing a ball}, or \textit{standing up}.\footnotemark[4]

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{materials_methods/Ofli2013BerkeleyDatabase}
	\caption{\textit{Throwing a ball} class of the MHAD dataset, seen from each camera group and the two Kinects with both views: \ac{RGB} and \ac{RGB-D}. \extracted{Ofli2013BerkeleyDatabase}}
\label{fig:Ofli2013BerkeleyDatabase}
\end{figure}

\subsection{ADL}
\cite{Pirsiavash2012DetectingViews} With medical applications in mind, authors present a first person view dataset of daily activity long-term videos. In fact, the label taxonomy was designed in terms of medical rehabilitation evaluations, in order to serve for future developments in the field. Another proposed application is to help in life-logging tasks for memory-loss patients. 

Concerning the data, each video includes the object bounding boxes with their labels and its tracking, the interactions that happen in the video, positioning of the hands in the scene, and the activity classes. We can observe this annotations in \referenced{Pirsiavash2012DetectingViews}.

The dataset grows up to 10 hours of \ac{RGB} video (more than a million frames) without scripted actions, in a division of 18 classes, including: \textit{combing hair},\textit{ make up}, \textit{brushing teeth}, \textit{laundry}, \textit{washing dishes},\textit{ making coffee}, \textit{vacuuming} or \textit{ using cell}.\footnotemark[5]

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{materials_methods/Pirsiavash2012DetectingViews}
	\caption{Sample ADL frame with multiple objects and arms detected. \extracted{Pirsiavash2012DetectingViews}}
\label{fig:Pirsiavash2012DetectingViews}
\end{figure}

\subsection{NTU RGB+D}
\cite{Shahroudy2016NTUAnalysis} Since many action datasets which include depth tend to have a small number of videos and classes, and with few actors, authors present a new large-scale \ac{RGB-D} action dataset which covers more than 56000 videos corresponding to a total of 60 classes, differentiated in three large groups: daily actions, health, and mutual ones. Moreover, videos are recorded from 80 different positions with the Kinect v2 camera, being able to obtain infrared data apart from \ac{RGB-D} images and skeleton joints. We can observe some of the actions in \referenced{Shahroudy2016NTUAnalysis}

Regarding the actors, the videos present a total of 40 of them, whose ages range from 10 to 35. This solves another common issue on previous datasets, since a lack of actor diversity could affect intra-class variation.

On the other hand, they propose two types of model evaluation, cross-subject and cross-view evaluation. The former divides the subjects in half, obtaining two groups of 20 subjects. One for training and the other for testing. The later divides the different camera views also in training and test groups. For the first one, the front and side views are included, while for the second one, views are from left and right sides with an angle of 45 degrees.\footnotemark[6]

\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth]{materials_methods/Shahroudy2016NTUAnalysis}
	\caption{From left to right: \ac{RGB}, \ac{RGB} with joints, depth, depth with joints, and infrared versions of the same frame. \extracted{Shahroudy2016NTUAnalysis}}
\label{fig:Shahroudy2016NTUAnalysis}
\end{figure}

\subsection{UCF101}
\cite{Soomro2012UCF101:Wild} Given the limited number of \ac{RGB} action datasets that included realistic scenes (without actors or prepared environments) and a wide range of classes until 2012, authors of this paper propose a new large-scale datasets of user-uploaded videos (YouTube). These present a much diverse type of challenges than the ones of previous datasets, since recordings can contain different lighting configurations, image quality degradation, cluttering, movement of the camera, and occluded scenes.    

In regards to the size of the dataset, 13320 videos are divided into 101 classes that cover five action groups: Human-Human Interaction, Sports, Playing Musical Instruments, Human-Object Interaction, and Body-Motion only. The actions contained in the first and fourth groups can be observed in \referenced{Soomro2012UCF101_Wild}.\footnotemark[7]

Furthermore, this dataset marked a milestone in what large scale action recognition datasets refers. It made possible to establish a well-known starting testbed to be improved as well as for benchmarking. Moreover, deep learning competitions where established around it, such as the different modalities of the THUMOS Challenge, which was run for three years in a row. After that, other large-scale datasets appeared, expanding the characteristics of the \ac{UCF101}. For this, marking the start of an ever-growing number of diverse large-scale action recognition datasets, the \ac{UCF101} dataset is also worth of.  

\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth]{materials_methods/Soomro2012UCF101_Wild}
	\caption{Classes for the Human-Object Interaction (blue) and Body-Motion Only (red) action groups from the \ac{UCF101} dataset. \extracted{Soomro2012UCF101:Wild}}
\label{fig:Soomro2012UCF101_Wild}
\end{figure}

\subsection{STAIR Actions}
\cite{Yoshikawa2018STAIRActions} Centering its attention in the task of action recognition and understanding at home scenarios, and its possible applications like threat detection or health-care, authors present a large-scale daily action \ac{RGB} video dataset with 100 daily-centered house actions grouped into five categories: object manipulation, washroom related, solo action, kitchen related, and multiplayer action. Each one can appear in 900 to 1200 videos, making a total of 102462 of them in the dataset. Regarding the video sources, either they have been downloaded from YouTube or provided by volunteers. Moreover they were labeled via crowdsourcing, with a program exclusively developed for this dataset.\footnotemark[8]

\subsection{EPIC-Kitchens}
\cite{Damen2018ScalingDataset} With similar intentions to the ones of the creators of the \ac{ADL}\cite{Pirsiavash2012DetectingViews} dataset, the authors of this work present a large-scale \ac{RGB} action dataset of first-person view kitchen actions, consisting of more than 11.5 millions of frames and 55 hours of recordings. These are divided into 39596 action segments pertaining to one of over 149 classes. On the other hand, each sample is annotated with bounding boxes of the objects used previous to, while, and after carrying out the action, obtaining a total of 454158 bounding boxes and 323 classes of objects. An example of the information contained in each video can be observed in \referenced{Damen2018ScalingDataset}.
 
In terms of evaluation, three types of challenges are proposed: Action recognition, action anticipation, and object detection. For this, also two types of data splits are provided: The seen kitchens, where they appear in both training (80\% approx.) and testing (20\% approx.) sets. And the unseen kitchen, in this case the recordings of one kitchen appear only in a single set, training or testing. This split, although only accounts for around the 7\% of frames in the dataset is challenging enough for modern deep learning models.\footnotemark[9]

\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth]{materials_methods/Damen2018ScalingDataset}
	\caption{Subsequent actions with corresponding object detections. \extracted{Damen2018ScalingDataset}}
\label{fig:Damen2018ScalingDataset}
\end{figure}

% Footnotes
\footnotetext[1]{Project page: \url{https://publish.illinois.edu/multimodalvisualanalytics/dataset/}}
\footnotetext[2]{Project page: \url{http://cvrc.ece.utexas.edu/KinectDatasets/HOJ3D.html}}
\footnotetext[3]{Project page: \url{http://cvrc.ece.utexas.edu/KinectDatasets/FirstPerson.html}}
\footnotetext[4]{Project page: \url{http://tele-immersion.citris-uc.org/berkeley_mhad}}
\footnotetext[5]{Project page: \url{https://www.csee.umbc.edu/~hpirsiav/papers/ADLdataset/}}
\footnotetext[6]{Project page: \url{http://rose1.ntu.edu.sg/datasets/actionrecognition.asp}}
\footnotetext[7]{Project page: \url{http://crcv.ucf.edu/data/UCF101.php}}
\footnotetext[8]{Project page: \url{https://stair-lab-cit.github.io/STAIR-actions-web/}}
\footnotetext[9]{Project page: \url{https://epic-kitchens.github.io/2018}}

\subsection{Dataset summary}

Here we present a summary table for the above datasets (see \ref{fig:DatasetSummary}). For each one, it contains the most useful information that we usually need for determining if a dataset suits our needs when coming to the action recognition problem. They are the following: The name of the dataset, the type of action the videos contain, the kind of data, whether if it is only in color or also includes depth, the number of classes and videos, the total amount of recording hours the dataset contains, the average number of videos per class, the \ac{FPS} at which the videos where captured, and the minimum, maximum and average length of each video in seconds. 

\begin{table}[H]
\label{fig:DatasetSummary}
\centering
\resizebox{\linewidth}{!}{

\begin{tabular}{@{}lcccccccccc@{}}
\toprule
\multicolumn{1}{c}{Name}  & Action          & Data  & Classes & Videos & Hours  & Vids./Class & FPS & Min s. & Max s. & Avg s. \\ \midrule
UTK-FPH    & Basic body      & RGB-D & 9       & 177    & -      & 19         & 30  & -         & -         & -         \\
UTK-FPNH & Basic body      & RGB-D & 9       & 189    & -      & 21         & 30  & -         & -         & -         \\
NTU RGB+D                 & General         & RGB-D & 60      & 56880  & 40     & 948        & -   & -         & -         & 2,53      \\
RGBD-HDA              & Daily (indoor)  & RGB-D & 12      & 1189   & 46     & 99         & 30  & 30        & 150       & 139,28    \\
UTK-A3D         & Basic body      & RGB-D & 10      & 200    & 0,06   & 20         & 30  & 0,17      & 4         & 1,04      \\
MHAD                      & Basic body      & RGB-D & 11      & 660    & 1,37   & 60         & 11  & -         & -         & 7,45      \\
UCF101                    & General         & RGB   & 101     & 13320  & 26,68  & 137        & 25  & 1,06      & 71,04     & 7,21      \\
ADL                       & Daily (indoor)  & RGB   & 18      & -      & 10     & -          & 30  & -         & -         & 99,50     \\
STAIRS                    & Daily (indoor)  & RGB   & 100     & 102462 & 156,54 & 1024       & -   & -         & -         & 5,5       \\
EPIC-K             & Daily (kitchen) & RGB   & 149     & 39594  & 55     & 265        & 60  & -         & -         & 5,00      \\ \bottomrule
\end{tabular}
}
\caption{Detailed summary table of the presented datasets.}

\end{table}


\vspace*{\fill}

\newpage
\section{Hardware}
\label{sec:materials_and_methods:hardware}

As one of the main key points for successfully obtaining high quality deep learning models is the need of vast amounts of data, the time taken for training the network with such quantities, can potentially become a bottleneck in the pipeline. To relieve from this, \textit{Asimov} server was built and set-up for obtaining great performance and energy efficiency.

The complete configuration of the hardware is specified in Table \ref{table:asimov}. Mainly, the server relies on three NVIDIA \acp{GPU} for accomplishing the previously proposed goal. The Titan X is devoted to deep learning, whilst a Tesla K40 was also installed for scientific computation purposes thanks to its exceptional performance with double precision floating point numbers. In addition, a less powerful GT730 was included for visualization and offloading the Titan X from that burden.

With respect to the software, the server runs Ubuntu $16.04$ LTS with Linux kernel $4.4.0-21-$generic for x86\_64 architecture. The GPU are running on NVIDIA driver version $361.42$ and CUDA $7.5$. 

Other relevant software includes Caffe RC$3$, PCL $1.8$ (master branch \texttt{NUM}), Vtk $5.6$, and Boost $1.58.0.1$. All libraries and tools were compiled using GCC $5.3.1$ and the CMake $3.5.1$ environment with release settings for maximum optimization.

\begin{table}[H]
	\centering
	\begin{tabular}{ll}
		\hline
		\multicolumn{2}{c}{Asimov}\\
		\hline
		Motherboard & Asus X99-A \footnotemark[0]\\
								& ~ Intel X99 Chipset\\
								& ~ $4\times$ PCIe $3.0/2.0 \times16 (\times16, \times16/\times16, \times16/\times16/\times8)$\\
		CPU & Intel(R) Core(TM) i7-5820K CPU @ 3.30GHz \footnotemark[1]\\
						& ~ $3.3$ GHz ($3.6$ GHz Turbo Boost)\\
						& ~ $6$ cores ($12$ threads)\\
						& ~ $140$ W TDP\\
		GPU (visualization) & NVIDIA GeForce GT730 \footnotemark[2]\\
												& ~ $96$ \acs{CUDA} cores\\
												& ~ $1024$ MiB of DDR3 Video Memory\\
												& ~ PCIe 2.0\\
												& ~ $49$ W TDP\\
		GPU (deep learning) & NVIDIA GeForce Titan X \footnotemark[3]\\
												& ~ $3072$ \acs{CUDA} cores\\
												& ~ $12$ GiB of GDDR5 Video Memory\\
												& ~ PCIe 3.0\\
												& ~ $250$ W TDP\\
		GPU (compute) & NVIDIA Tesla K40c \footnotemark[4]\\
									& ~ $2880$ \acs{CUDA} cores\\
									& ~ $12$ GiB of GDDR5 Video Memory\\
									& ~ PCIe 3.0\\
									& ~ $235$ W TDP\\
		RAM & $4\times8$ GiB Kingston Hyper X DDR4 $2666$ MHz CL$13$\\
		Storage (Data) & (\acs{RAID}1) Seagate Barracuda 7200rpm 3TiB SATA III \acs{HDD} \footnotemark[5]\\
		Storage (OS) & Samsung 850 EVO 500GiB SATA III \acs{SSD1} \footnotemark[6]\\
		\hline
	\end{tabular}
	\caption{Hardware specifications of Asimov.}
	\label{table:asimov}
\end{table}

\footnotetext[10]{\url{https://www.asus.com/Motherboards/X99A/specifications/}}
\footnotetext[11]{\url{http://ark.intel.com/products/82932/Intel-Core-i7-5820K-Processor-15M-Cache-up-to-3_60-GHz}}
\footnotetext[12]{\url{http://www.geforce.com/hardware/desktop-gpus/geforce-gt-730/specifications}}
\footnotetext[13]{\url{http://www.geforce.com/hardware/desktop-gpus/geforce-gtx-titan-x}}
\footnotetext[14]{\url{http://www.nvidia.es/content/PDF/kepler/Tesla-K40-PCIe-Passive-Board-Spec-BD-06902-001_v05.pdf}}
\footnotetext[15]{\url{http://www.seagate.com/es/es/internal-hard-drives/desktop-hard-drives/desktop-hdd/?sku=ST3000DM001}}
\footnotetext[16]{\url{http://www.samsung.com/us/computer/memory-storage/MZ-75E500B/AM}}

\newpage

The server was configured for remote access using \ac{SSH}. The installed version is OpenSSH $7.2$p$2$ with OpenSSL $1.0.2$. Authentication based on public/private key pairs was configured so only authorized users can access the server through an \ac{SSH} gateway with the possibility to forward X11 through the \ac{SSH} connection for visualization purposes. Figure \ref{fig:asimov_ssh} shows an \ac{SSH} connection to Asimov.

At last, a mirrored \ac{RAID}1 setup was configured with both \acp{HDD} for optimized reading and redundancy to tolerate errors on a data partition to store all the needed datasets, intermediate results, and models. The \ac{SSD1} was reserved for the operating system, swap, and caching.

\begin{figure}[!t]
	\centering
	\includegraphics[width=0.65\textwidth]{materials_methods/ssh}
	\caption{Asimov's \acs{SSH} banner message and welcome screen with server info.}
	\label{fig:asimov_ssh}
\end{figure}

\subsection{Docker}
Like in any kind of research, when conducting an experiment, an environment that is both deterministic and reproducible is needed, for that, the conditions in which it is run should be constant and known. In the field of computer science, this can be translated to first, obtaining the same expected output after each program execution, and second, that this happens with a pre-established set of system specifications and software versions. Moreover, is of our interest for the setting-up process, being simple and fast.

For achieving this purpose, several approaches exists. For example, in the case the program was written in Python, virtual environments could be used, being able to easily manage which libraries are installed in each environment and with which version (it can be done with \textit{pip} or a manager like \textit{Conda}). Although for other languages similar solutions exists, Conan\footnotemark[17] for C++, maven-env\footnotemark[18] for Java or use Python virtualenv for more than one language\footnotemark[19], these are either little mature or somewhat obscure.

The question becomes more complex when experiment dependencies extend to more than one language and the the global environment is shared across different users, such as in a server host. In this case, the most straightforward solution could be to fully virtualize an operative system, install the needed dependencies and run our experiments in isolation from other users. A well know approach for these purposes, exist under the form of virtual machines. These are both, easy to use and create, but with the downside of having some performance overheads and lacking the ability to fully use all the machine potential.

One robust solution that meets all the requirements, follows the same virtualization lines and can make use of the available hardware without noticeably diminishing the performance is Docker\footnotemark[20].

This software is based in the concept of containers, which in contrast to virtual machines, where the virtual operative systems are managed by an hypervisor platform (Virtual Box, Parallels, Virtual PC...) on top of the host operative system, they run in the same space as any other program (user space), being able to access the kernel as these programs do, resulting in effective performance gains and start-up times (of seconds or milliseconds). As it can be understood, Docker is a container engine, offering a wide range of tools for building, managing and deploying them.

For creating and running each container, Docker utilizes images, which hold the description of the actual state of it. This can be analogically compared to classes (images) and object instances at runtime (containers). Furthermore, this concept can be extended to the means of inheritance, as each change in an image can be seen as a new layer added onto it, thus resulting in a new one (child class).

\begin{figure}[H]
	\centering
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[width=0.9\textwidth]{materials_methods/docker_layers}
		\caption{Docker container.}
		\label{subfig:docker_layers}
	\end{subfigure}
	\hfill
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[width=0.9\textwidth]{materials_methods/vm_layers}
		\caption{Virtual machine.}
		\label{subfig:vm_layers}
	\end{subfigure}
	\caption{Representation of abstraction layers on Docker and Virtual machines applications.}
	\label{fig:docker_vm_layers_comparison}
\end{figure}

\newpage

Indeed, this mechanism works similarly to how Git tracks changes, looking at the differences between the new file version and the past one, resulting in an appreciable disk space saving. Moreover, this derives into an abstraction where images can be managed using commands that result near to the ones of a control version system: Images can be both, pulled from other ones, extending their contents, and pushed, updating them. They are kept in either a local registry or a remote one, Docker Hub\footnotemark[21]. And even more, containers can be committed into the same image or into a new one. Also, the preserved changes in one of them can be rolled back. Finally, and greatly interesting for the kind of research machine learning conducts, the Docker engine officially supports the use of NVIDIA \acp{GPU}\footnotemark[22], being able to run models inside containers directly onto them, without apparent differences. \newline

\footnotetext[17]{\url{http://blog.conan.io/2016/08/04/Conan-virtual-environments-Manage-your-C-and-C++-tools.html}}
\footnotetext[18]{\url{https://github.com/AlejandroRivera/maven-env}}
\footnotetext[19]{\url{http://jsatt.com/blog/virtualenvs-for-all/}}
\footnotetext[20]{\url{https://www.docker.com/}}
\footnotetext[21]{\url{https://hub.docker.com/}}
\footnotetext[22]{\url{https://github.com/NVIDIA/nvidia-docker}}







