\chapter{Experiments}
\label{cha:experiments}

\begin{chapterabstract}
In this fourth chapter we propose three methods for accelerating a video action recognition system. Furthermore, their validity is explored with different experiments. In Section \ref{sec:experiments:tools}, we briefly introduce the three proposals. Then, in Section \ref{sec:experiments:tools_decoding} we study the possibility of \ac{GPU} video decoding for accelerating the action recognition pipeline. Another proposal, infer optical flow with \acp{CNN} is explained in Section \ref{sec:experiments:tools_ir}. In Section \ref{sec:experiments:tools_proof}, we propose use the \ac{TSN} framework as the third improvement. Moreover we combine it with the first one in order to show its viability. Finally, in Section \ref{sec:experiments:tools_conclusion} we summarize the experiment results.
\end{chapterabstract}

\section{Introduction}
\label{sec:experiments:tools}

In the following sections, we present three approaches, which can also be combined, for improving the time performance of current action recognition systems, in order to orient them for real-time applications. 

First, the use of \ac{TSN} framework as the main network architecture, since it can provide us with high quality accuracy at inference time by using a small number of video frames. Secondly, for alleviate the I\textbackslash O bottleneck that appears when working with large video datasets, we propose the use of video decoding applications through the \ac{GPU}. For this we will make sure that we the frames do not see their image quality decreased. Third, extract optical flow by using \acp{CNN} for a more efficient and action recognition adapted implementation. Specifically, we will use the MotionNet architecture from \ac{HTS}. Since it is implemented in Caffe, we will need to find a proper tool that allows use to convert the model into the chosen framework, PyTorch. Moreover, we will check the amount of precision loss that the conversion induces.

Finally, we will test the first and third proposals be training on a reduced set of the \ac{UCF101} dataset, and conclusions will be extracted.

\section{GPU Video Decoding}
\label{sec:experiments:tools_decoding}

Since the beginning of the modern deep learning era, data storing and loading times have always been a bottleneck in the pipeline. Although recently we are witnessing great speedups thanks to new hardware technologies like \ac{SSD1} for storing, or data transferring devices (between \ac{CPU} to \ac{GPU}, and vice-versa) such as NVLINK, the issue persists. 

Many of the research areas where this problem aggravates more are the ones which work with videos as the main dataset source. These include: predictive learning, video understanding, question answering, activity recognition, and super-resolution networks, between many others.

The main approach when tackling this problem in those areas is to first extract all the frames for each video of the dataset, for example by using FFmpeg\footnotemark[15], and save them in a high quality image format, rather than one with possible loosy compression and artifact generations, in order to properly train the network. This comes with an increasing need of storage space, since the more information willing to be kept, the larger in size our converted image dataset will be. 

In the \referenced{frame_sizes} we can see the effects of storing the \ac{UCF101} dataset, composed of only 13320 videos in different formats. For the case of JPEG (image), it occupies 63 GiB, while in AVI format (video) it occupies 9.25 times less, 6.8 GiB. If it is transformed to the proper MP4 format, needed by \ac{NVVL} (the video decoder we are going to use) with the corresponding number of frames, it occupies 14.2 GiB, still 4.44 times less. If we take this into a fine-grained level, such as frames, we can see that the storage differs by a large margin.

\begin{figure}[H]
	\centering
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[width=1\textwidth]{experiments/size_gb}
		\caption{Storage occupied in GiB.}
		\label{subfig:size_gb}
	\end{subfigure}
	\hfill
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[width=1\textwidth]{experiments/size_frame_kb}
		\caption{Mean size per frame.}
		\label{subfig:size_frame_kb}
	\end{subfigure}
	\caption{Storage comparison between frames and video formats for the \ac{UCF101} dataset.}
	\label{fig:frame_sizes}
\end{figure}

In order to alleviate this problem, a useful solution is to directly load video files into memory, decode the necessary frames, prepare them, and finally feed them to the network. Actually, \acp{API} that can manage the first two steps exist: FFmpeg\footnotemark[16] libraries itself, and higher abstraction modules like PyAV\footnotemark[17] or PIMS\footnotemark[18], which both load data into the \ac{CPU}. On the other hand, the (beta) Hwang\footnotemark[19] project, also supports NVIDIA \acp{GPU} through the use of their specific hardware decoder units. 

Furthermore, those designed with machine learning tasks in mind, which can provide all the mentioned steps have been recently developed. Two are currently known: Lintel\footnotemark[20]\cite{Duke2018Lintel:Decoding}, and \ac{NVVL}\footnotemark[21]\cite{Casper2018NVVL:Loader}. The first focuses on \ac{CPU} loading (uses FFmpeg as backend), while the second targets \ac{GPU} devices. Indeed, although being written in C++, it offers off-the-shelf PyTorch modules (dataset and loader). Moreover, another wrapper for CuPy\footnotemark[22] arrays has been created\footnotemark[23]. 

Regarding performance, we can see that it reduces by a large margin the I/O processing times, as it can be appreciated in \referenced{imageLikeEmbed_2}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{experiments/imageLikeEmbed_2}
	\caption{Average loading time (milliseconds) that 32-Floating Point PyTorch tensors take to be available in the GPU. The experiment was run on an NVIDIA V100 \ac{GPU} over one epoch with batches of size 8. \extracted{Casper2018NVVLDatasets}}
	\label{fig:imageLikeEmbed_2}

\end{figure}

More benchmarks that take into account memory usage and CPU loads can also be found in the blog post, while an even more detailed evaluation is located on GitHub\footnotemark[25].

Regarding data, loading behaves like a sliding window of stride one, where frame sequences of a previously fixed length are subsequently loaded and returned as a single tensor. On the other hand, we can apply different transformations to these sequences: data type (float, half, or byte), width and height resizing and scaling, random cropping and flipping, normalizing, color space (\ac{RGB} or \ac{YCbCr}), and frame index mapping.

\footnotetext[15]{\url{http://ffmpeg.org/}}
\footnotetext[16]{\url{https://github.com/FFmpeg/FFmpeg}}
\footnotetext[17]{\url{https://mikeboers.github.io/PyAV/}}
\footnotetext[18]{\url{http://soft-matter.github.io/pims/v0.4/video.html}}
\footnotetext[19]{\url{https://github.com/scanner-research/hwang}}
\footnotetext[20]{\url{https://github.com/dukebw/lintel}}
\footnotetext[21]{\url{https://github.com/NVIDIA/nvvl}}
\footnotetext[22]{\url{https://cupy.chainer.org/}}
\footnotetext[23]{\url{https://github.com/mitmul/pynvvl}}
\footnotetext[24]{\url{https://devblogs.nvidia.com/acclerate-machine-learnng-nvvl/} \label{nvvl_blog}}
\footnotetext[25]{\url{https://github.com/NVIDIA/nvvl/tree/master/pytorch/test}}

Furthermore, we can compare the difference between the original frames and the ones loaded through \ac{NVVL}. For this task, we are going to use the \ac{SSIM} index between two pictures, usually used in the video industry for measuring the visual difference we can perceive when comparing frames of an original and downsampled video. It ranges from 0 to 1, where 1 is given for two identical pictures and 0 for two completely different ones. For example, given the two frames obtained from the UCF dataset (\textit{Diving} class) that can be observed in \referenced{frames}, we can notice a green band on the right extreme of the NVVL loaded image.

\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth]{experiments/frames}
	\caption{Original frame (left) and NVVL obtained frame (right). The frames pertain to a sample of the \textit{Diving} class in the \ac{UCF101} dataset}
	\label{fig:frames}

\end{figure}

Apart from this, we can not perceive any other substantial degradation in quality. Indeed, the \ac{SSIM} obtained is 0.992, indicating that this artifact is probably due to a bug rather than a low quality video processor. For reassuring this fact, we can compute the \ac{SSIM} heat map, in order to locate other possible missed artifacts:

\begin{figure}[H]
	\centering
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[width=1\textwidth]{experiments/frames_heatmap}
		\caption{Overview.}
		\label{subfig:frames_heatmap}
	\end{subfigure}
	\hfill
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[height=0.4\textheight]{experiments/frames_heatmap_closer}
		\caption{Green band zoom in.}
		\label{subfig:frames_heatmap_closer}
	\end{subfigure}
	\caption{Heat map of above frames, the lighter the color, the closer to the original frame each pixel is.}
	\label{fig:frames_heatmap_and_closer}
\end{figure}

\newpage

Thus we can assume that there will be no harm at the time of incorporating this tool in a neural network training pipeline.  

Now, we should pay attention to knowing which is the current time speedup we can obtain from substituting the image loading system of the \ac{TSN} framework by a \ac{NVVL} pipeline. For this, after adapting the frame-index generation functions, and integrating the video loader into it, we can perform the following:

\begin{enumerate}
\item Obtain a list of videos, get the total number of videos and the mean number of frames per video.
\item Extract all the frames from the videos, also convert them into the required \ac{NVVL} video format.
\item Select the number of frames per video that are going to be loaded. For \ac{NVVL}, all the frames have to be loaded.
\item Measure how much time it takes for extracting the told number of frames (into the \ac{GPU}) on each occasion. For \ac{NVVL} this only needs to be done once.
\item Obtain mean times and trend for the previous process and compare the results.
\end{enumerate}

\paragraph{Step 1.\\\\}
We will use the first 450 videos of the UCF split-1 train list obtained with the data tools provided by the \ac{TSN} framework. This list is formated with a row for each video. In each row, the path to the video, the number of frames the video has and its class index. Due to this, the total number of frames can be obtained just by summing the second element of each row over the whole list. The resulting number is 87501. So the mean number of frames in a video is \approx 194.

\paragraph{Step 2.\\\\}
For completing this step, we can simply follow the instructions and commands provided in the repositories of each project. We have to take into account that the extraction process can take a quite greater amount of time than the video converting process.

\paragraph{Step 3.\\\\}
In this case, we are going to load even number of frames, starting from 3 and finishing in 25, a total of 12 different instances. This has been selected since the authors of \ac{TSN} test the model with 3, 5, 7, and 9 frames per video.

\paragraph{Step 4.\\\\}
For obtaining an accurate measurement, we are going to repeat each execution 29 times. For computing the time we have used Python \textit{time.time} function. Also, in order to free all the resources in each run, we are going to loop inside a bash script instead of inside the Python executing script itself, thus having the process killed automatically.

\paragraph{Step 5.\\\\}
In this step we computed the mean values for each number of frames. The time taken for loading all the videos with \ac{NVVL} is \approx 24.18 seconds. On the other hand, we can plot the results obtained from loading sole frames:

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{experiments/frames_sec}
	\caption{Mean loading time in seconds of each number of frames executed (blue). Trend line of from the obtained data (red). Y axis represents the loading time in seconds, while the X axis shows the number of frames used.}
	\label{fig:trend}
\end{figure}

We can notice that the trend follows a lineal growth with respect to the number of frames loaded. Since we computed the equation defining the trend line (shown in the lower-right part of \referenced{trend}), we can obtain a more precise approximation of the speedup achieved when using \ac{NVVL}. For this, since we know the number of frames loaded with \ac{NVVL} is the same as the mean number of frames obtained in \textbf{Step 1}, we just need to substitute it in the equation (X variable), obtaining a mean value of \approx 458.74 seconds or 7.65 minutes. We have achieved an improvement on loading time performance, leading to a 18.97 times speedup when using \ac{NVVL}.

\section{Converting Caffe networks into PyTorch}
\label{sec:experiments:tools_ir}

In order to be able to use the pre-trained Caffe MotionNet in PyTorch, we need to convert both the model whose format is \textit{.prototxt} and its weights (\textit{.caffemodel}) to a PyTorch understandable version. Usually this is not a simple task, since both frameworks do not share neither the same model representations nor the file saving format. 

A quite popular solution for solving this problem is the use of model converters, i.e, programs that alleviate the work load, and commonly with a few clicks and commands allow us to transfer a network in a source framework to a destination one. Two characteristics of these tools have to be taken into account: (1) Most of the converters are community-driven, (2) due to precision aspects (e.g. floating point arithmetic), the same experiments, but run on the destination network, can vary their output (rarely by a significant amount).

Regarding the first point, this leads to a situation similar to the one of high-level and assembly languages in the past, when intermediate representations did not exist and programmers created compilers exclusively for bridging the gap only between one pair, thus creating a confusing and vast amount of them. Currently, this is happening again, since the number of new and useful deep learning frameworks is constantly rising. Indeed, projects that keep track of the most effective converters do exist, one of the most popular ones is \textquot{Deep learning model convertor}, located at GitHub\footnotemark[26]. 

On the other hand, current projects aimed at creating a standardized intermediate representation (an abstraction of this concept can be observed in \referenced{mmdnn_ir}), backed by well known organizations are coming into existence. Specifically, two of them stand out of the crowd: \ac{MMdnn}\footnotemark[27] and \ac{ONNX}\footnotemark[28]. The first one compiles a set of tools for conversion, model visualization, model compatibility testing, and code fragments for training and predicting generation. The second, apart from conversion and model visualization, also focuses on providing hardware optimization at time of exporting to its intermediary representation format. 

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{experiments/mmdnn_ir}
	\caption{Deep learning frameworks interoperability concept (from \ac{MMdnn}).}
	\label{fig:mmdnn_ir}
\end{figure}

At the moment, ONNX only allows model exporting for PyTorch, and MMdnn although supporting it for Caffe, fails to properly convert the MotionNet model. Therewith, a pair Caffe to PyTorch converter has been chosen. Concretely, it first started as a set of Darknet \ac{YOLO} converters\footnotemark[29] (to and from Darknet, Caffe, and PyTorch). Later, the Caffe to PyTorch converter was maintained as a standalone project named \textquot{caffe2pytorch}\footnotemark[30].

This tool has the particularity of converting the model at runtime, instead of generating the model and network files in the destination save format. It does so by looping over the \textit{.prototxt} file, reading the layers and creating the corresponding PyTorch versions of each one, keeping the same parameters or using the default ones if the equivalent layers are absent. Then, it proceeds similarly with the weights, through the use of the Numpy library.

\newpage

For checking the error this converter can induce into the network, two metrics are used: \ac{MSE} and absolute subtraction error. They are used for comparing how much the outputs of the original and converted network differ. For this, random inputs are fed to both networks in a 100,000 iterations loop. For each of these repetitions, the mean is computed and plotted in a graph, which can be observed in \referenced{converter_errors}. At the end, a minimal error is obtained. Being \approx 3.23e-14 for MSE and \approx 1.34e-07 for the absolute subtraction error.

\begin{figure}[H]
	\centering
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{experiments/errors_mse}
		\caption{\ac{MSE} measure.}
		\label{subfig:docker_layers}
	\end{subfigure}
	\hfill
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{experiments/errors_subtraction}
		\caption{Absolute subtraction measure.}
		\label{subfig:vm_layers}
	\end{subfigure}
	\caption{As we can see, both measures follow the same trend, stabilizing around the iteration 20,000.}
	\label{fig:converter_errors}
\end{figure}

Therefore, we successfully managed to convert MotionNet Caffe model into its PyTorch version assuring that no precision loss happened.

% Footnotes
\footnotetext[26]{\url{https://github.com/ysh329/deep-learning-model-convertor}}
\footnotetext[27]{\url{https://github.com/Microsoft/MMdnn}}
\footnotetext[28]{\url{https://github.com/onnx}}
\footnotetext[29]{\url{https://github.com/marvis/pytorch-caffe-darknet-convert}}
\footnotetext[30]{\url{https://github.com/marvis/pytorch-caffe}}

\section{Proof of concept: Training RGB TSN with NVVL}
\label{sec:experiments:tools_proof}

So far we have shown how useful incorporating \ac{NVVL} into a video-consuming deep learning pipeline can be, it allows us to reduce both the storage and data transfer costs at the same time we do not suffer degradation in image quality. Now, what only remains is to incorporate this tool into a common action recognition scenario, where we train and test a network for learning to categorize human actions.

Such a network is going to be \ac{TSN}, since it has demonstrated a superior performance in the task at hand. Moreover, we propose to make use of the converted \ac{HTS} Caffe model and weights, in order to avoid pre-computing the optical flow and being able to use \ac{NVVL} also in this stream, focusing the resulting pipeline for real-time applications. In spite of the dataset we are going to use, memory limitations detailed below, and time constrains, we are going to focus the following experiment only for the \ac{RGB} stream.

Before starting, we need to prepare the data into a format that is compatible with \ac{NVVL}. As referred in the GitHub repository (link above), we need videos with either H.264 or HEVC (H.265) \acp{codec}, and yuv420p pixel format, also they can be in any container that FFmpeg parser supports.

\newpage

Moreover, we have to take into account the number of keyframes each video will have, i.e., a \ac{codec} only contains a subset of all the frames that we see in a video, these are the keyframes. At the time of decoding, the rest of the frames are obtained by algorithmically inferring them through the keyframes. For this reason, when loading sequences that can start and end at any frame (similar to what we can do with \ac{NVVL}), the system has to seek the nearest keyframe, which can be far before or after the starting frame. This can result into an underperformant execution, and for this reason when converting the videos, we have to indicate the frequency of keyframes per frame we want to have. 

\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{experiments/keyframes}
	\caption{Representation of how keyframes can be evenly inserted into a video stream.}
	\label{fig:keyframes}
\end{figure}


Developers of the video loader suggest to set one keyframe in intervals that correspond to the length of the sequences we are going to load. For example, if we are going to load sequences of length 7, then every 7 frames there will be a keyframe. Furthermore, they also provide the required commands to carry out this conversion with FFmpeg.

For our case, we are going to set every frame in the video to be a keyframe, this is due to the fact that currently the PyTorch wrapper (the C++ API seems more flexible) is intended for loading multiple frame sequences for each video with a sliding window approach of a fixed length. Although this length could be equal to the number of frames in the video, thus loading only a sequence per video, this would only work if all the videos had the same length, since this parameter, the sequence length, is global for the whole dataset.

For iterating over the dataset, we are going to use the data loader provided by \ac{NVVL} PyTorch wrapper, where in each iteration it will load a batch of frame sequences. Since now, each frame is a sequence of length one, we need to set the batch size also to one. In this way we can easily know when the loader has fully output a video, add it to a list, and when we have enough videos, group them in a batch of the size we want for providing it to the network. Furthermore, for accomplishing this we also need to set to false the shuffle option in the loader.

Although we are ready for training our network, an impediment arises at the time of writing this work. Whether the videos have not been properly converted, or there is a code issue, the data loader seems to get silently stuck when loading some videos (more on these here\footnotemark[31]). For solving this, one circumvention is to create a loader for each video instead of having one for the whole dataset. 

% Footnotes
\footnotetext[31]{\url{https://github.com/NVIDIA/nvvl/issues/29}}

So far this works, but what happens next is that \ac{GPU} memory is not properly freed, thus limiting the size of our dataset to the space available on the graphic card at the moment. For Asimov, this concurs in having around 240 videos for training and 160 for validation (only the Titan card supports \ac{NVVL}).

For this, following the same lines of motivation proposed at the beginning of the document, we are going to select daily actions for the reduced dataset we can work with. Specifically, it is composed of eight classes from the Human-Object Interaction group of the \ac{UCF101} dataset: \textit{Apply Eye Makeup}, \textit{Apply Lipstick}, \textit{Blow Dry Hair}, \textit{Brushing Teeth}, \textit{Cutting In Kitchen}, \textit{Mopping floor}, \textit{Shaving Beard}, and \textit{Typing}. The training set contains 30 videos for each action, while the validation one has 20 of them.

Regarding the training hyper-parameters, we are going to use the ones set by default for the \ac{TSN} with the only exception of the batch size and number of epochs. For the former, we have set it to 4 due to the limited memory, for the later, we will perform 40 epochs, which is enough for the model to converge with this dataset. For the metrics, we will keep track of the loss and top-1 and top-5 accuracies for both the training and validation sets.

Once all the epochs have been completed, we finish our training with in a common situation, 100\% of top-1 (and top-5) accuracy and zero loss. Clearly, our network has overfitted. This is due to the scarce amount of data and its limited variability. Moreover, such deep networks (\ac{BN} Inception) are proner to overfit, since they have more flexibility (more number of parameters) for adjusting to the data they are consuming while in training. However, we obtain validation accuracies of 76.25\% and 98.125\% for the top-1 and top-5 versions respectively, with a loss of \approx 1.52. Taking into account the data we are working with, these results are quite promising in comparison to what has happened in the training phase.

Now, we can get more insights if looking on how the training and validation have evolved. In the figure \ref{fig:train_curves}:

\begin{figure}[H]
	\centering
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{experiments/train_ac1}
        \caption{Top-1 training accuracy.}
		\label{subfig:train_ac1}
	\end{subfigure}
	\hfill
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{experiments/train_ac5}
		\caption{Top-5 training accuracy.}
		\label{subfig:train_ac2}
	\end{subfigure}
    \hfill
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{experiments/train_loss}
		\caption{Training loss.}
		\label{subfig:train_loss}
	\end{subfigure}
	\caption{Training curves for 40 epochs, 60 iterations per epoch.}
	\label{fig:train_curves}
\end{figure}

Here we can take note of two facts. First, the top-5 accuracy converges much faster (iter. \approx 200) than the top-1 accuracy (iter. \approx 800). Clearly, this is something that can bee foreseen, since it takes more time to learn the label of a video rather than guess it among five samples.
Secondly, we see that the unsmoothed curve (shaded red) bounces between higher and lower values (accuracy and loss) among the first iterations. This effect can be better seen in the validation curves:

\begin{figure}[H]
	\centering
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{experiments/val_ac1}
        \caption{Top-1 validation accuracy.}
		\label{subfig:val_ac1}
	\end{subfigure}
	\hfill
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{experiments/val_ac5}
		\caption{Top-5 validation accuracy.}
		\label{subfig:train_ac2}
	\end{subfigure}
    \hfill
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{experiments/val_loss}
		\caption{Validation loss.}
		\label{subfig:val_loss}
	\end{subfigure}
	\caption{Validation curves for 40 epochs, 60 iterations per epoch.}
	\label{fig:val_curves}
\end{figure}

This happens as a consequence of the small batch size we have previously set. The smaller is the batch size, the more number of weight updates we will perform. If it is to small, we could find the following:

\begin{itemize}
\item Instability: The frequent updates will cause the metrics to wander, going continually up and down.
\item Not meaningful updates: The reduced number of samples makes it to contain less information about the error (negative gradient) direction in each update, thus needing a major number of epochs for converging into the same accuracy than with a bigger batch size. This can be summarized as longer training times.
\item Hit a local minima: Also known as plateau, and commonly induced by the previous statements, a small batch size can make that the network gets stuck on a non-optimum (nor sub-optimum) minimum of the loss function, obtaining insufficient performance results.
\end{itemize}

As intuition, we can take a look at the figure \ref{fig:batch_sizes}, where on the left, the evolution of three types of batch size loss curves (arriving to the minimum) are plotted. The blue one, represents a batch of the same size of the dataset, thus making only one update per epoch, a smother curve with a much less noisy evolution. Although it seems the best approach, the detail is in the time and space it takes to update the weights, since we have a large number of samples, we have to compute a vast amount of operations. Moreover, commonly is impracticable that a complete dataset fits into a modern \ac{GPU} memory.

The purple curve, is for the case where we realize one-sample updates, something that reflects, on extreme, what happened during the training of our network. Finally, the green curve shows the daily situation of most deep learning trainings, where the batch size is found in balance with the number of updates per epoch. Although there are frequent updates, they are not too much for firing divergence, at the same time a reasonable time is taken for finding the error.

\begin{figure}[H]
	\centering
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{experiments/batch_size_effect}
		\caption{Different batch size methods for finding the minimum (red).}
		\label{subfig:batch_size_effect}
	\end{subfigure}
	\hfill
	\begin{subfigure}{0.4\textwidth}
		\centering
		\includegraphics[width=\textwidth]{experiments/batch_error}
		\caption{Relationship between bath sizes and error for a sample dataset. Y axis is accuracy and X axis is the epoch.}
		\label{subfig:batch_error}
	\end{subfigure}
	\caption{Effects of batch sizes when training.}
	\label{fig:batch_sizes}
\end{figure}

In order to better visualize where the network guesses right or wrong, we can make use of the training and validation confusion matrices \ref{fig:cm}, where in each cell we can see the percentage of true positives for the class in the cell row. For example, in the validation matrix, 35\% percent of the times we see the class \textit{Brushing teeth}, the network sees it as \textrm{Shaving Beard}. Moreover, we can note that by obtaining the trace of a confusion matrix (summation over the diagonal) and dividing by the number of classes, we retrieve the final accuracy.

% Matrices
\begin{figure}[H]
	\centering
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{experiments/cm_train}
		\caption{Training matrix.}
		\label{subfig:cm_train}
	\end{subfigure}
	\hfill
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{experiments/cm_val}
		\caption{Validation matrix.}
		\label{subfig:cm_val}
	\end{subfigure}
	\caption{Confusion matrices for the proposed dataset.}
	\label{fig:cm}
\end{figure}

Easly, we notice what we determined before, the training set is overfitted, since for all diagonal cells the confusion matrix reports a 100\% value (normalized between 0 and 1). On the other hand, when analyzing validation matrix, we can see that the network mostly fails when the classes are very similar. For example:

\begin{itemize}
\item \textit{Apply Eye Makeup} is confounded 15\% of the times with \textit{Apply Lipstick}, since both use some kind of hand stick and cover zones of the face vertically near between each other, is logic to think that they are more difficult to differentiate.
\item \textit{Apply Eye Makeup} and \textit{Shaving Beard} follow a similar error pattern, since in both actions there is hand movement over the zone of the mouth and arm movement around the whole face.
\end{itemize}

In other cases, the contrary can happen, when the action is easily differentiable from others, this mostly happens with two actions, \textit{Mopping the floor} which usually happens in a room, and \textit{Cutting in kitchen} where the camera focuses on the knife and the cutting table area.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{experiments/ok_wr}
	\caption{Class labels and network predictions: First line is correct label, second line is the predicted one, green if correct or red if not.}
	\label{fig:keyframes}
\end{figure}


\section{Conclusion}
\label{sec:experiments:tools_conclusion}
As we can see, it is clear that all three proposals are good candidates for selection as a way for speeding up the action recognition pipeline. On the other hand, some of them are still not mature enough for being considered in a production environment. Notwithstanding, they have shown its effectivity. For this, we should consider these technologies as a promising starting point that will allow us build and pave the way towards a more efficient and robust real-time deep learning action recognition system.





