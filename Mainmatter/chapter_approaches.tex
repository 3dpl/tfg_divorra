\chapter{Approaches}
\label{cha:approaches}

\begin{chapterabstract}
In this third chapter we examine the most recent works of action recognition carried out during the past three years. Setting in section \ref{sec:experiments:context_approaches} their context. Afterwards, in Section \ref{sec:experiments:papers}, important action recognition works related with deep learning are introduced. Then, in Section \ref{sec:experiments:approaches} we explain in detail the ones more suited for real time action recognition. We conclude this chapter with Section \ref{sec:experiments:selected_approaches} with an explanation of the approaches selected for experimentation.
    
\end{chapterabstract}

\section{Introduction}
\label{sec:experiments:context_approaches}
In the following sections we review the most modern action recognition works carried out in the past three years. Mainly extracted from the most important computer vision and deep learning conferences: ICCV, ECCV, CVPR, NIPS, BMVC. Both non real-time and real-time systems are taken into consideration.

\section{Overview: New action recognition approaches}
\label{sec:experiments:papers}

In this section we briefly introduce modern notable works that that try to solve the task of action recognition. Although they not suitable for real-time applications, they are worth for taking a clever approach and their innovation. Moreover, for many of them, since their implementation is public, a link to the corresponding project pages has been added as a footnote.

\paragraph{First-Person Activity Forecasting with Online Inverse Reinforcement Learning\\\\}

\cite{RhinehartFirst-PersonLearning} This paper presents a novel method for predicting future behaviors by modeling the interactions between the subject, objects, and their environment, through a first person mounted camera. The system\footnotemark[7] makes use of online inverse reinforcement learning. Thus making it possible to continually discover new long-term goals and relationships. 

\begin{figure}[H]
	\centering
	\includegraphics[width=0.55\textwidth]{experiments/RhinehartFirst-PersonLearning}
	\caption{Environment map with possible future goals states (blue). Histograms show the probabilities of which object has been acquired (left) and the long-term goal (right).}
\label{fig:RhinehartFirst-PersonLearning}
\end{figure}


\paragraph{Joint Prediction of Activity Labels and Starting Times in Untrimmed Videos\\\\}

\cite{MahmudJointVideos} By following a similar approach to that of the hybrid Siamese networks, this paper shows that is possible to simultaneously predict future activity labels and their starting time. It does so by taking advantage of features of previously seen activities and currently present objects in the scene.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{experiments/MahmudJointVideos}
	\caption{Pipeline representation of the proposed joint prediction approach.}
\label{fig:MahmudJointVideos}
\end{figure}

\paragraph{Online Real-time Multiple Spatiotemporal Action Localisation and Prediction\\\\}

\cite{SinghOnlinePrediction} Thanks to the use of \acp{SSD2} \acp{CNN}, the system proposed in this paper is capable of predicting both action labels, and their corresponding bounding boxes in real-time (28\ac{FPS}). Moreover, it can detect more than one action at the same time. All of this is accomplished by using \ac{RGB} image features combined with optical flow ones (with a decrease in the optical flow quality and global accuracy) extracted in real-time for the creation of multiple action tubes\footnotemark[8].

\paragraph{Deep Sequential Context Networks for Action Prediction\\\\}
\cite{KongDeepPrediction} In this case, for predicting action class labels before the action finishes, authors make use of features extracted from fully observed videos processed at train time, for filling out the missing information present in the incomplete videos to predict. Furthermore, thanks to this approach their model obtains a great speedup improvement when compared to similar methods.

\paragraph{Visual Forecasting by Imitating Dynamics in Natural Sequences\\\\}
\cite{ZengVisualSequences} A model that is capable of performing visual forecasting at different abstraction levels is presented in this paper. For example, the same model can be trained for future frame generation as well as for action anticipation. This is accomplished by following an inverse reinforcement learning approach. Also, the model is enforced to imitate natural visual sequences from pixel level.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{experiments/ZengVisualSequences}
	\caption{Example of predictions (second row) over the moving \ac{MNIST} dataset.}
\label{fig:ZengVisualSequences}
\end{figure}

\paragraph{Real-Time RGB-D Activity Prediction by Soft Regression\\\\}
\cite{HuReal-TimeRegression} The model developed in this paper is capable of predicting in real-time future activities labels on \ac{RGB-D} videos. This is accomplished by making use of soft regression, for jointly learning both the predictor model and the soft labels. Moreover real-time performance (around 40\ac{FPS}) is obtained by including a novel \ac{RGB-D} feature named \ac{LAFF}. 

\paragraph{Temporal Convolutional Networks for Action Segmentation and Detection\\\\}
\cite{LeaTemporalDetection} The authors present a new type of convolutional networks called \ac{TCN} which are used for action detection and segmentation. By using a hierarchical convolutional layer approach, the model is capable of extracting long-range temporal patters.

Moreover, a \ac{TCN} Encoder-Decoder system is built for performing the mentioned tasks. After training, it is able to surpass current similar approaches. Furthermore, the system presents a better performance than \acp{Bi-LSTM} networks\footnotemark[9].

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{experiments/LeaTemporalDetection}
	\caption{Diagram of the Encoder-Decoder architecture.}
\label{fig:LeaTemporalDetection}
\end{figure}

\paragraph{SST: Single-Stream Temporal Action Proposals\\\\}

\cite{BuchSST:Proposals} In this paper, a system that is capable of presenting temporal action proposals on a video with only one forward pass is presented. Thus there is no need to create overlapped temporal sliding windows. Moreover, the system can work with long untrimmed videos of arbitrary length in a continued fashion. Finally, by combining the system with action classifiers, temporal action detection performance is increased.

This is achieved by extracting visual features with a \ac{C3D} network and feeding them to a \ac{GRU} layer which encodes the sequential information of the actions and outputs a fixed number of proposals at each timestep. The best proposals are selected by applying standard post-processing techniques\footnotemark[10].

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{experiments/BuchSST_Proposals}
	\caption{Representation of the developed system.}
\label{fig:BuchSST_Proposals}
\end{figure}

\paragraph{Quo Vadis, Action Recognition? A New Model and the Kinetics Dataset\\\\}

\cite{CarreiraQuoDataset} Apart from reviewing current action classification techniques, authors of this paper present a new convolutional model, known as \ac{I3D}, which is used as a spatio-temporal feature extractor. After this, authors pre-train \ac{I3D} based models on the Kinetics dataset, showing that with this approach, action classification performance on well known datasets is noticeably increased.

For achieving this results, different techniques are core in an \ac{I3D} model. First, inflation of 2D convolutional modules into 3D ones (such as filters and pooling kernels). Generally, this is done by just converting NxN filters to its cubic form of NxNxN. Second, parameters pre-trained on ImageNet models are bootstrapped for being adapted to their own inflated versions, this is done by training the 3D versions on videos composed by the same single image repeated several times. Third and last, train two \ac{I3D} systems independently, one with \ac{RGB} videos and another with optical flow video features, and at test time, average their predictions\footnotemark[11].

\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth]{experiments/CarreiraQuoDataset}
	\caption{Inflated Inception-V1 architecture (left) and detailed Inflated Inception, \textit{Inc.}, module (right).}
\label{fig:CarreiraQuoDataset}
\end{figure}

\paragraph{Spatiotemporal Multiplier Networks for Video Action Recognition\\\\}

\cite{FeichtenhoferSpatiotemporalRecognition} In this paper, a fully space-time convolutional two-stream network (named ST-ResNet) is proposed for the task of action recognition in videos. The first stream is fed with \ac{RGB} data while the second, with optical flow features. The main particularity of this model is the existing interconnections between both streams. Moreover, for learning long-term relationships, identity mapping kernels are injected through the network. All of this allows the network to predict on a single forward pass.

Specifically, the network takes as base for both streams a \ac{ResNet}-like architecture, in which features computed as output from a \ac{ResNet} block of the motion stream (the one with optical flow features) are passed to the next \ac{ResNet} block of the appearance (the \ac{RGB}) stream, by multiplying it with the input of the \ac{RGB} block. Thus creating a gating-like mechanism when the gradient is computed. On the other hand, for being able to support larger time intervals, 1D convolutions in conjunction with feature space transformations are applied through the network\footnotemark[12].

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{experiments/FeichtenhoferSpatiotemporalRecognition}
	\caption{Detail of the modified \ac{ResNet} block, with the gating moving from the motion to the appearance stream.}
\label{fig:FeichtenhoferSpatiotemporalRecognition}
\end{figure}

\paragraph{Predictive-Corrective Networks for Action Detection\\\\}

\cite{DavePredictive-CorrectiveDetection} A new kind of recurrent neural network, known as predictive-corrective network, is presented in this paper. Authors make use of it for solving the problem of action detection in videos obtaining satisfactory results. In its basis the model: (1) Focuses on changes between frames, (2) predicts the future, (3) makes corrections upon it by observing what truly happens next.

Paying attention to variations, instead of to the actual frames, allows the model to reduce the number of computations done, as two subsequent frames could present high correlation between each other. Probably introducing needless repeated information.

In detail, the memory of the \acp{RNN} are the activations computed from the \ac{CNN} layers one timestep before. Moreover, for allowing the network to correctly perform predictive-corrective computations, the memory of the \acp{RNN} is re-initialized every few frames (for example, every three frames). Furthermore, memory is also accessed to subtract it from the actual computed \ac{CNN} activations. Comparisons with current methods show that predictive-corrective networks can compete with models based on two-stream approaches, without the need of making use of optical-flow features\footnotemark[13].

\paragraph{Asynchronous Temporal Fields for Action Recognition\\\\}

\cite{SigurdssonAsynchronousRecognition} Authors of this paper propose a model that is capable of detailedly reason about aspects of an activity, i.e, for each frame the model is capable of predicting the current activity, its action and object, the scene, and the temporal progress. This is accomplished by making use of \acp{CRF} that are fed by \ac{CNN} feature extractors. Moreover, for being able to train this system in and end-to-end-manner, an asynchronous stochastic inference algorithm is developed. 

Looking at the architecture of the model, it can be divided in two parts. First, the \ac{CNN} extractor, which follows a dual stream approach, where \ac{RGB} and optical flow features are computed, both using a \ac{VGG16}-like structure. Second, the asynchronous temporal field (developed by the authors), which is in charge of time reasoning and predicting, making use of the features extracted by the two-stream network. Particularly, this is a fully connected \ac{CRF}, structured for temporal modeling. Furthermore, for analyzing the results obtained with this architecture, a \ac{t-SNE} embedding is computed for the intents of the predicted videos, showing that similar videos are clearly clustered together\footnotemark[14].


% Footnotes
\footnotetext[7]{Project page: \url{http://www.cs.cmu.edu/~nrhineha/darko.html}}
\footnotetext[8]{GitHub repository: \url{https://github.com/gurkirt/realtime-action-detection}}
\footnotetext[9]{GitHub repository: \url{https://github.com/colincsl/TemporalConvolutionalNetworks}}
\footnotetext[10]{GitHub repository: \url{https://github.com/shyamal-b/sst}}
\footnotetext[11]{GitHub repository: \url{https://github.com/deepmind/kinetics-i3d}}
\footnotetext[12]{GitHub repository: \url{https://github.com/feichtenhofer/st-resnet}}
\footnotetext[13]{GitHub repository: \url{https://github.com/achalddave/predictive-corrective}}
\footnotetext[14]{GitHub repository: \url{https://github.com/gsig/temporal-fields}}

\section{Real-time approaches}
\label{sec:experiments:approaches}

In this section we further detail three outstanding modern works that utilize deep learning for solving the question of action recognition in real time scenarios. Moreover, their code is also publicly available, and the corresponding links have been added as footnotes.

\paragraph{Temporal Segment Networks for Action Recognition in Videos\\\\}
In this work \cite{Wang2017TemporalVideos}, authors propose a \ac{CNN} framework for the recognition of actions in videos, both trimmed an untrimmed. Along with it, a series of guidelines for properly initialize and operate such deep models for this task are proposed.

The framework aims to tackle four common limitations when using convolutional networks on videos. First, the difficulty of using long-range frame sequences, due to high computational and memory space costs, which can lead to miss important temporal information. Second, most of the systems focus on trimmed videos instead of untrimmed ones (several actions may happen in a video). Adapting to these would mean to properly localize actions and avoid background (irrelevant) video parts. Third, meanwhile deep models become complex, still many datasets are small in number of samples and diversity, lacking enough data for properly train them and avoid over-fitting. Fourth, time consumed for optical-flow extraction can become a delay for both, using large-scale datasets and using the model on real-time applications.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{experiments/Wang2017TemporalVideosA}
	\caption{Representation of TSN framework. First a snippet is extracted from each of a fixed number of segments that equally divide the video, Then, features such as optical flow or \ac{RGB-diff} (top and bottom images of the second process column) are extracted. After passing through the corresponding stream, an aggregation function joins the individual snippet class probabilities. Then, softmax is applied for obtaining the final video action class.}
\label{fig:Wang2017TemporalVideosA}
\end{figure}

Tackling the first point, authors propose the use of a sparse temporal sampling instead of dense one, being able to avoid redundant information consecutive frames could carry. Specifically, the video is divided in a constant number part from which a random snippet (short frame sequence) is extracted. For each snippet the same set features are extracted (\ac{RGB}, optical flow, \ac{RGB-diff}) and passed to its corresponding \ac{CNN} stream. After obtaining the predictions for each snippet, a selected global merging function (named by the authors as \textit{segmental consensus function}) is applied over all the snippets for joining the partial predictions. Lastly, through softmax the final action class is computed. These functions are applied for each action class. They include either max pooling or average pooling between the class prediction values of all the snippets. Other two are: Top-K pooling, which combines the previous two functions by obtaining the average over the K greater class prediction values. And attention or linear weighting, in which both a weighted sum over the class prediction values is performed, having these weights being learned together with the network parameters.

Briefly, for the case of untrimmed videos (which diverges from the task of this work), they are divided in a fixed number of windows with the same duration in which the previous mentioned action recognition process is applied. Then, for obtaining the list of predicted actions taking place in the video, one of the two advanced functions (attention weighting or top-K pooling) that can reduce the importance of sequence background and focus on the action instances is applied. This approach served as a determining factor for obtaining the first place of the untrimmed videos category at ActivityNet Large Scale Activity Recognition Challenge 2016.

\newpage

On the third aspect, the guidelines are introduced: (1) Cross-modality initialization, where pre-trained weights on the \ac{RGB} model are transferred to other kind of streams (optical flow and \ac{RGB-diff}) through a series of simple transformations; (2) partial batch normalization by freezing all this kind of layers values (mean and variance) except the first one on the pre-trained models for properly adapting to the different data distributions of the other streams. To further prevent overfitting, a high dropout ratio is applied after where the global pooling takes place; (3) data augmentation operations, in which apart from horizontal flipping and random cropping, two new ones are applied: Corner cropping, where only the center of the image or corner sections is presented to the network, for avoiding it to only pay attention on the first area. And second, modified a scale-jittering for the task action recognition.

With respect to implementation details, different architectures are used: Inception v2 and v3 or ResNet-152. Regarding the input of the temporal stream, three approaches are investigated: Optical flow, warped optical flow (both using height and width axes) aimed at reducing camera motion influence and focus on the action subject, and \ac{RGB-diff} (of pixel intensities between two consecutive frames) which as indicated above, are intended to speed-up the motion representation at the same time they maintain a correct accuracy.

Different datasets such as \ac{HMDB-51} and ActivityNet are used during the experiments. In these, the results on the combinations mentioned temporal streams are presented, showing that although the use of warped optical flow derives on better accuracy than common optical flow, the time performance is greatly decreased being almost three times slower than the later (from 14 to 5 \ac{FPS}). Finally, by using the \ac{RGB} channel along the \ac{RGB-diff} one, the network is able to predict in real-time (\ac{FPS} \textgreater \space 30) at more than 330 \ac{FPS} while maintaining a good quality performance ratio, helping this to solve the fourth previously mentioned limitation. Another investigated aspect is the number of segments used, resulting in 3 the minimal number for applying the framework (only one is understood as a common two-stream approach) and 7 the optimal one for capturing the temporal structure and avoid saturating (obtain the same accuracy with more segments) the network. Then, by using the \ac{RGB} and common optical flow combination with 7 segments, the aggregation functions are tested leading to the conclusion that for trimmed videos average pooling works best while for untrimmed ones both advanced functions give similar results. Moreover, the best performing architecture in the experiments results to be \ac{BN} Inception.

For having a better insight on how the network represents action classes, some of them (pertaining to \ac{HMDB-51} and \ac{UCF101}) are visualized in images by using a gradient ascent method (use an addition instead of a subtraction for updating the weights). When comparing the results, it can be seen that with \ac{TSN}, model center more its attention in the human motion in contrast to common two-streams which context motion influences more. \newline

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{experiments/Wang2017TemporalVideosB}
	\caption{Visualization of five \ac{UCF101} class representations. It can be seen, for example in the diving class, that when using \ac{TSN} more attention is paid to the swimmer (more of this action poses appear in the image) and less to the context motion for which the model lacking \ac{TSN} encourages more, being possible to see that more waves appear on its image.}
\label{fig:Wang2017TemporalVideosB}
\end{figure}

\paragraph{Encouraging LSTMs to Anticipate Actions Very Early\\\\}
The authors of this paper \cite{Aliakbarian2017EncouragingEarly} propose a multi-stage \ac{LSTM} architecture combined with a novel loss function, that is capable of predicting action class labels in videos, even when only the first frames of the sequence have been shown. The model takes advantage of action-aware and context-aware features for succeeding in this task.

Specifically, the anticipation loss function penalizes false positives more severely as the sequence progresses, this is because some actions are only truly differentiable after the first few frames, e.g., swimming and diving. On the other hand, the contrary is done for false positives, since the objective is to assign the correct class label sooner rather than later. 

The model bases the feature extraction process in \acp{CNN} while for interpreting the sequential information, uses \acp{LSTM}. For both context and action features, the same network, ImageNet pre-trained \ac{VGG16}, is shared until one of its last convolution layers, where a stream for each feature appears. 

For the context-aware, the rest of the \ac{VGG16} layers are kept until the last one where the number of outputs is adapted to the objective dataset (\ac{UT-Interaction}, \ac{JHMDB}, and \ac{UCF101}). The feature vector is extracted from the previous to the output dense layer, having the meaning of obtaining a representation of image scene. 

For the action-aware stream, first \acp{CAM}\cite{Zhou2016LearningLocalization} are extracted from the next convolutional layer, then after passing through dense layers, the feature vector is extracted in the same manner than the previous stream. In relation to \acp{CAM}, they as used for representing the image sections that contain more discriminative information about its class label. For this, they can indicate in which point of the image the action takes place.

Both streams are trained with still images before joining them to the \ac{LSTM} model (being frozen in this phase), which uses as input the extracted feature vectors (context and action) for each frame of the sequence. Indeed, these is so since results where nearly the same and it required a smaller computation cost.

For the recurrent part, the context features are passed through a \ac{LSTM} layer that diverges in two streams, the first is composed of a dropout function followed by an output softmax dense layer, where the anticipation loss is applied. The second, another \ac{LSTM} which also takes as input the action features by concatenating them with the output of the first \ac{LSTM}. Then, the same structure of the first stream is repeated in this one. Finally, the final loss is the result of adding the losses of both streams.

At time of predicting the correct action class, instead of obtaining a label for each frame (timestep) of the sequence, in order to make use of all their information, the final class is computed through average pooling of all the predictions up to the current frame.
	
On the results, the system can make correct predictions (over 80\% of accuracy) with seeing less than 30 frames of each video. Moreover, experiments show that the combination of the two feature types provides a noticeable performance increase. Also, as action features are extracted from informative parts of still images they are more robust to unrelated movement than other motion type features. At the time of visualizing how the information is interpreted by the convolutional networks, for each stream (after splitting), is extracted an image resulting of combining through average pooling all the channels of the first convolutional layer. In this, action features centre its attention on the subject while context ones this happens through the scene.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.50\textwidth]{experiments/Aliakbarian2017EncouragingEarly}
	\caption{Representation of context (second row) and action features (third row). In the archery picture we can see that the second type of features are more active around the hand grabbing the bow while the first type, is focused on the other body parts e.g., the flexed arm or the bow shape.}
\label{fig:Aliakbarian2017EncouragingEarly}
\end{figure}

\paragraph{Hidden Two-Stream Convolutional Networks for Action Recognition\\\\}
Another side for approaching the question of real-time action recognition can be found in \cite{Zhu2017HiddenRecognition}, where the use of a convolutional network for automatically compute optical flow is presented. 

More in detail, in a first phase, a \ac{CNN} denoted as MotionNet is trained unsupervisedly for the task of optical-flow estimation. After obtaining acceptable results similar to optimal traditional methods, the network is attached to a conventional \ac{CNN} as the temporal stream part of the whole model, being the spatial stream similar in architecture SURE¿ to the other one. Then, the network is trained (including MotionNet) on the task of action recognition from frame sequences. The approach enables the optical flow generator to be adapted to the characteristics of the task and further finding a suitable motion representation.

It is worth to mention that this approach is different from two other known methods that compute optical flow through \acp{CNN} both with supervised training. The first \cite{Yue2016ActionFlowNet:Recognition}, uses as expected optical flow the one computed through handcrafted methods, setting the quality of the real representations as a complex approachable milestone. The second method \cite{Ilg2017FlowNetNetworks}, is partially restricted also by the training datasets, since it is synthetic, which although it possesses a true ground truth, due to this nature, it can be limited to specific datasets and tasks.

Regarding MotionNet, it is taken the assumption that if by applying the inverse warping function to the computed optical flow for two subsequent frames the first frame could be obtained, then the network would have perfectly approximated the optical flow function. Thus, it uses this comparison as one of the main contributors to the global loss function. For the architecture, a common convolution/transposed-convolution network with skip connections is trained from zero, with the particularity of using small kernel sizes (seem to better capture small motions like the variations in two subsequent frames (optical flow)), strided convolutions instead of max pooling since, as stated by the authors, for lowering the dimensionality of images on tasks regarding densely predicting pixels can have adverse effects. Although existing architectures like \ac{VGG16} or ResNet have been tested, this one obtains the best balance between accuracy and real-time performance. On the other hand, both the temporal and spatial streams use the VGG-16 architecture. Furthermore, results show that the model performs better than \cite{Ilg2017FlowNetNetworks} while consuming much less memory space. Indeed, researchers implement a much less deep network, which although performs slightly worse than the original network, this time consumes around 70 times less. This can encourage to search for innovative architectures that are shallow rather than deep.

On the use of learned optical flow representation for action recognition, is found that still handcrafted features perform better (surpassing \cite{Ilg2017FlowNetNetworks} and \ac{HTS}). Finally, regarding the real-time aspect, different approaches are tested, leading to the conclusion that even when using the \ac{TSN} framework, the network can still achieve it. Also, the previously mentioned reduced version with this framework can run at more than 480 \ac{FPS}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{experiments/Zhu2017HiddenRecognition}
	\caption{Different optical flow representations. While the first column method (handcrafted) performs better, the last column \cite{Ilg2017FlowNetNetworks} extracts a more fine-grained version. Even with this, MotionNet still obtains a major performance while presenting noisy optical flow outputs. This, as authors indicate, can mean that the best motion representation still has to be found.}
	\label{fig:Zhu2017HiddenRecognition}

\end{figure}

\section{Selected approaches}
\label{sec:experiments:selected_approaches}
Since we are aiming to improve the speed of a current action recognition system, we only focus our selection on the last three works review. Among them, we will discard \citep{Aliakbarian2017EncouragingEarly}, although the concept of frame anticipation is effective, the model requires more complex computations due to the two recurrent architectures used. Also, managing the hidden state of the \ac{RNN} can become troublesome in the long run. For these, and since we need a lightweight model, we will focus the experiments carried out in the next chapter, on the \ac{TSN} and \ac{HTS} architectures. 







